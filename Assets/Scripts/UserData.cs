﻿using UnityEngine;
using System.Collections;

public class UserData : MonoBehaviour {


    public float height;
    public float neck;
    public float shoulders;
    public float chest;
    public float waist;
    public float hips;
    public float inseam;
    public float underBust;
    public float overBust;
	public float thighs;
	public float biceps;
    public string userName;
    public bool isMale;

	public UserData(bool isMale, float height, float neck, float shoulders, float chest, float waist, float hips, float inseam, float thighs, float underBust, float overBust) {
        
		this.height = height;
        this.neck = neck;
        this.shoulders = shoulders;
		this.waist = waist;
		this.hips = hips;
		this.inseam = inseam;
		this.thighs = thighs;
		this.overBust = overBust;
		this.underBust = underBust;
		isMale = false;
    }

	public UserData ( float height, float neck, float shoulders, float chest, float waist, float hips, float inseam, float biceps, float thighs, bool isMale){
		this.height = height;
		this.neck = neck;
		this.shoulders = shoulders;
		this.chest = chest;
		this.waist = waist;
		this.hips = hips;
		this.inseam = inseam;
		this.thighs = thighs;
		this.biceps = biceps;
		isMale = true;
	}


    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
