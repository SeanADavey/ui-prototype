﻿using UnityEngine;
using System.Collections;

public class ChangingRoom : MonoBehaviour {

	private int _charModelIndex = 0;

	private CharacterAsset ca;
	public string nameString;
	public GameObject male;
	public GameObject female;
	public GameObject UIMale;
	public GameObject UIFemale;




	private string _charModelName = "Male";

	// Use this for initialization
	void Start () {
		

		ca = GameObject.Find ("Character Asset Manager").GetComponent<CharacterAsset> ();

		InstantiateCharacterModel ();

	}


	void OnGUI(){
	
		ChangeCharacterMesh ();
	}


	private void ChangeCharacterMesh() {
	
		if (GUI.Button (new Rect (435, 500, 200, 40), _charModelName)) {
		
			_charModelIndex++;

			InstantiateCharacterModel ();
		
		}
	
	}


	// Update is called once per frame
	void Update () {
	
	}

	private void InstantiateCharacterModel() {

		switch (_charModelIndex) {

		case 1:
			_charModelName = "Female";

			break;
		default:
			_charModelName = "Male";
			_charModelIndex = 0;


			break;

		}

		if (transform.childCount > 0) {
		
			for (int cnt = 0; cnt < transform.childCount; cnt++) {
			
				Destroy (transform.GetChild(cnt).gameObject);
			}
		}
		if (_charModelIndex == 1) {
			female = Instantiate (ca.CharacterMesh [_charModelIndex], transform.position, Quaternion.identity) as GameObject;

			female.transform.parent = transform;

			female.transform.rotation = transform.rotation;
			UIFemale = GameObject.Find ("Female_target");
		} else {
			male = Instantiate (ca.CharacterMesh [_charModelIndex], transform.position, Quaternion.identity) as GameObject;

			male.transform.parent = transform;
			male.transform.localScale = new Vector3(3,3,3);

			male.transform.rotation = transform.rotation;
			UIMale = GameObject.Find ("Male_target");
		}
	}
}
