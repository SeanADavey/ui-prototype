﻿using UnityEngine;
using System.Collections;

public class BlendshapeSlider : MonoBehaviour {
	
	private float height { get; set; }
	private float heightPrevious { get; set;}
	public float neck { get; set;}
	public float shoulders { get; set;}
	public float chest { get; set;}
	public float waist { get; set;}
	public float hips { get; set;}
	public float inseam { get; set;}
	public float thighs { get; set;}
	public float biceps { get; set;}
	private bool isActive = false;
	public  GameObject male;

	private SkinnedMeshRenderer skinMeshRenderer;
	public GUIStyle  Style;
    public GUIStyle Title;


	// Use this for initialization
	void Start () {

		height = 50.0f;
		heightPrevious = 50.0f;
		neck = 50.0f;
		shoulders = 50.0f;
  	    chest = 50.0f;
  	    waist = 50.0f;
  	    hips = 50.0f;
  	    inseam = 50.0f;
 	    thighs = 50.0f;
  	    biceps = 50.0f;
		isActive = true;

		skinMeshRenderer = GetComponent<SkinnedMeshRenderer>();
		Style.normal.textColor = Color.white;
        Style.fontSize = 20;
        Title.fontSize = 30;
        Title.normal.textColor = Color.white;
        GUI.backgroundColor = Color.white;






    }

    void OnGUI()
	{
		

        /*Initialising GUI elements*/

        //Title 
		/*
        GUI.Label (new Rect (60, 30, 200, 50), "Set up your avatar", Title);

		height = GUI.HorizontalSlider(new Rect(80 , 100 , 200 , 30) , height , 1.0F , 99.0F);
		GUI.Label(new Rect(150 , 75 , 200 , 20), "Height" , Style);

		neck = GUI.HorizontalSlider(new Rect(80 , 160 , 200 , 30) , neck , 1.0F , 99.0F);
		GUI.Label(new Rect(155 , 135 , 200 , 20), "Neck" , Style);

		shoulders = GUI.HorizontalSlider(new Rect(80, 220 , 200 , 30) , shoulders , 1.0F , 99.0F);
		GUI.Label(new Rect(135 , 195 , 200 , 20), "Shoulders" , Style);

		chest = GUI.HorizontalSlider(new Rect(80, 280 , 200 , 30) , chest , 1.0F , 99.0F);
		GUI.Label(new Rect(150 , 255 , 200 , 20), "Chest" , Style);

		waist = GUI.HorizontalSlider(new Rect(80, 340 , 200 , 30) , waist , 1.0F , 99.0F);
		GUI.Label(new Rect(150 , 315 , 200 , 20), "Waist" , Style);

		hips = GUI.HorizontalSlider(new Rect(80, 400 , 200 , 30) , hips , 1.0F , 99.0F);
		GUI.Label(new Rect(160 , 375 , 200 , 20), "Hips" , Style);

		inseam = GUI.HorizontalSlider(new Rect(80, 460 , 200 , 30) , inseam , 1.0F , 99.0F);
		GUI.Label(new Rect(145, 435 , 200 , 20), "Inseam" , Style);

		thighs = GUI.HorizontalSlider(new Rect(80, 520, 200, 30), thighs, 1.0F, 99.0F);
        GUI.Label(new Rect(145, 495, 200, 20), "Thighs", Style);

		biceps = GUI.HorizontalSlider(new Rect(80, 580, 200, 30), biceps, 1.0F, 99.0F);
        GUI.Label(new Rect(145, 555, 200, 20), "Biceps", Style);
		*/

    }
	public void heightTest() {
		if (isActive){

			float heightDiff = (height - heightPrevious);
			heightDiff = heightDiff / 250;


			if (height >= 50.0f) {
				
				//Arm adjustments for above average height
				GameObject.Find ("Bip001 L UpperArm").transform.Translate (heightDiff * -1.0f, heightDiff * 0.3f, 0, null);
				GameObject.Find ("Bip001 R UpperArm").transform.Translate (heightDiff, heightDiff * 0.3f, 0, null);

				GameObject.Find ("Bip001 L ForeTwist").transform.Translate (0, heightDiff * 0.1f, heightDiff * 0.5f, null);
				GameObject.Find ("Bip001 R ForeTwist").transform.Translate (0, heightDiff * 0.1f, heightDiff * 0.5f, null);

				GameObject.Find ("Bip001 L ForeTwist").transform.Translate (heightDiff * 0.1f, heightDiff * 0.3f, heightDiff * 0.5f, null);
				GameObject.Find ("Bip001 R ForeTwist").transform.Translate (heightDiff * -0.1f, heightDiff * 0.3f, heightDiff * 0.5f, null);

				GameObject.Find ("Bip001 L ForeTwist2").transform.Translate (heightDiff * -0.1f, heightDiff * 0.3f, 0, null);
				GameObject.Find ("Bip001 R ForeTwist2").transform.Translate (heightDiff * -0.1f, heightDiff * 0.3f, 0, null);

				GameObject.Find ("Bip001 L ForeTwist2").transform.Translate (heightDiff * 0.1f, heightDiff * 0.3f, 0, null);
				GameObject.Find ("Bip001 R ForeTwist2").transform.Translate (heightDiff * -0.1f, heightDiff * 0.3f, 0, null);

				GameObject.Find ("Bip001 L ForeTwist2").transform.Rotate (0, 0, heightDiff * -125.0f);
				GameObject.Find ("Bip001 R ForeTwist2").transform.Rotate (0, 0, heightDiff * -125.0f);

				GameObject.Find ("Bip001 L Hand").transform.Translate (heightDiff * 0.3f, heightDiff * 0.7f, 0, null);
				GameObject.Find ("eip001 R Hand").transform.Translate (heightDiff * -0.3f, heightDiff * 0.7f, 0, null);
			

			} else if (height < 50) {

				//modding arm movement after height adjustment when less than 170cm

				GameObject.Find ("Bip001 L UpperArm").transform.Translate (heightDiff * -1.2f, heightDiff * 0.3f, 0, null);
				GameObject.Find ("Bip001 R UpperArm").transform.Translate (heightDiff * 1.2f, heightDiff * 0.3f, 0, null);
	            
				GameObject.Find ("Bip001 L ForeTwist").transform.Translate (heightDiff * 0.2f, heightDiff * 0.3f, heightDiff * 0.5f, null);
				GameObject.Find ("Bip001 R ForeTwist").transform.Translate(heightDiff * -0.2f, heightDiff * 0.3f, heightDiff * 0.5f, null);

	            GameObject.Find ("Bip001 L ForeTwist").transform.Translate (heightDiff * 0.2f, heightDiff * 0.3f, heightDiff * 0.5f, null);
				GameObject.Find ("Bip001 R ForeTwist").transform.Translate (heightDiff * -0.2f, heightDiff * 0.3f, heightDiff * 0.5f, null);

	            GameObject.Find ("Bip001 L ForeTwist2").transform.Translate (heightDiff * 0.2f, heightDiff * 0.3f, heightDiff * 0.1f, null);
				GameObject.Find ("Bip001 R ForeTwist2").transform.Translate (heightDiff * -0.2f, heightDiff * 0.3f, heightDiff * 0.1f, null);

				GameObject.Find ("Bip001 L ForeTwist2").transform.Translate (heightDiff * 0.2f, heightDiff * 0.3f, heightDiff * 0.5f, null);
				GameObject.Find ("Bip001 R ForeTwist2").transform.Translate (heightDiff * -0.2f, heightDiff * 0.3f, heightDiff * 0.5f, null);

				GameObject.Find ("Bip001 L Hand").transform.Translate (heightDiff * 0.1f, heightDiff * 0.7f, heightDiff * 0.25f, null);
				GameObject.Find ("eip001 R Hand").transform.Translate (heightDiff * -0.1f, heightDiff * 0.7f, heightDiff * 0.25f, null);

	            
	            GameObject.Find("Bip001 L Hand").transform.Translate(heightDiff * -0.2f, heightDiff * -0.3f, heightDiff * -0.2f, Space.Self);
	            GameObject.Find("eip001 R Hand").transform.Translate(heightDiff * -0.2f, heightDiff * -0.3f, heightDiff * 0.2f, Space.Self);

	            GameObject.Find("Bip001 L ForeTwist2").transform.Translate(0, heightDiff * -0.3f, 0, Space.Self);
	            GameObject.Find("Bip001 R ForeTwist2").transform.Translate(0, heightDiff * -0.3f, 0, Space.Self);

	            GameObject.Find("Bip001 L ForeTwist1").transform.Translate(0, heightDiff * 0.2f, 0, Space.Self);
	            GameObject.Find("Bip001 R ForeTwist1").transform.Translate(0, heightDiff * 0.2f, 0, Space.Self);
	            
			}
	    }

		heightPrevious = height;
	}

    

	// Update is called once per frame
	void Update () {

		//Change weighting on blendshape variables
		skinMeshRenderer.SetBlendShapeWeight(0,height);
		skinMeshRenderer.SetBlendShapeWeight(1,neck);
		skinMeshRenderer.SetBlendShapeWeight(2,shoulders);
		skinMeshRenderer.SetBlendShapeWeight(3,chest);
		skinMeshRenderer.SetBlendShapeWeight(4,waist);
		skinMeshRenderer.SetBlendShapeWeight(5,hips);
		skinMeshRenderer.SetBlendShapeWeight(6,inseam);
		skinMeshRenderer.SetBlendShapeWeight(7, thighs);
		skinMeshRenderer.SetBlendShapeWeight(8, biceps);

		if (height != heightPrevious) {
			heightTest();
			
		}
	


	}

}
