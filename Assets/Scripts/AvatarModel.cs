﻿using UnityEngine;
using System.Collections;

public class AvatarModel : MonoBehaviour {

	public bool isMale;
	public Component[] models;
	GameObject maleRig;
	GameObject femaleRig;
	string meshName;
	private SkinnedMeshRenderer skin;
	private CharacterAsset ca;
	private int charModelIndex;


	// Use this for initialization
	void Start () {
		isMale = true;
		meshName = "Male";
		models = GetComponentsInChildren <Animator> (true);
		maleRig = GameObject.Find ("Male Rig");
		femaleRig = GameObject.Find ("Female Rig");
		femaleRig.SetActive (false);
		ca = GameObject.Find ("Character Asset Manager").GetComponent<CharacterAsset> ();

	}

	void Awake () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnGUI () {

		if (GUI.Button (new Rect (200, 150, 200, 40), meshName)){
			isMale = !isMale;
			switchMesh (isMale);
		}

		if (GUI.Button (new Rect (300, 300, 50, 50), "Reset")){
			Reset();
		}

	}

	void switchMesh(bool gender){
		if (gender) {
			meshName = "Male";
			maleRig.SetActive (true);
			femaleRig.SetActive (false);

		} else {
			meshName = "Female";
			maleRig.SetActive (false);
			femaleRig.SetActive (true);

		}
	}

	void Reset () {
		isMale = true;
		meshName = "Male";

		//RESET SCRIPT

}
}


