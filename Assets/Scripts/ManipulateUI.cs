﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ManipulateUI : MonoBehaviour 
{
	private Vector3 restPosition;
	private Vector3 inUsePosition;


	// Use this for initialization
	void Start () 
	{
		restPosition = GetComponent<RectTransform>().position;
		inUsePosition = new Vector3 (transform.position.x - 551.1f, transform.position.y, transform.position.z);
	}

	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnMouseOver()
	{
		GetComponent<Image> ().color = Color.blue;
		Vector3.Lerp (restPosition, inUsePosition, Time.deltaTime * 2);
	}
}
