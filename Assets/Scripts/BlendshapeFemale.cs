﻿using UnityEngine;
using System.Collections;

public class BlendshapeFemale : MonoBehaviour {

	public float defaultNum { get; set; }
	public float height { get; set; }
	public float neck { get; set; }
	public float shoulders { get; set; }
	public float underBust { get; set; }
	public float waist { get; set; }
	public float hips { get; set; }
	public float overBust { get; set; }
	public float inseam { get; set; }
	public float thighs { get; set; }
	public float biceps { get; set; }

	public float heightPrevious = 50.0f;
	public float heightDiff;

	private SkinnedMeshRenderer skinMeshRenderer;

	public GUIStyle Style;
    public GUIStyle Title;


	public string stringToEdit = "";

	// Use this for initialization
	void Start () {
				
		defaultNum = 50.0f;
	    height = 50.0f;
	    neck = 50.0f;
	    shoulders = 50.0f;
	    underBust = 50.0f;
	    waist = 50.0f;
    	hips = 50.0f;
   	 	overBust = 50.0f;
  	   	inseam = 50.0f;
    	thighs = 50.0f;
     	biceps = 50.0f;

		skinMeshRenderer = GetComponent<SkinnedMeshRenderer> ();
		Style.normal.textColor = Color.white;
        Style.fontSize = 20;
        Title.fontSize = 30;
        Title.normal.textColor = Color.white;
		GUI.backgroundColor = Color.white;
    }

	void OnGUI () {
		



        GUI.Label(new Rect(60, 100, 200, 50), "Set up your avatar", Title);

		height = GUI.HorizontalSlider(new Rect(80 , 230 , 200 , 30) , height, 34.1f , 99.0f );
		GUI.Label(new Rect(150, 205 , 200 , 20), "Height" , Style);

		neck = GUI.HorizontalSlider(new Rect(80 , 295 , 200 , 30) , neck, 34.1F , 99.0F);
		GUI.Label(new Rect(155 , 270 , 200 , 20), "Neck" , Style);

		shoulders = GUI.HorizontalSlider(new Rect(80, 360 , 200 , 30) , shoulders, 34.1f , 99.0f);
		GUI.Label(new Rect(130 , 335 , 200 , 20), "Shoulders" , Style);

		underBust = GUI.HorizontalSlider(new Rect(80, 425 , 200 , 30) , underBust, 34.1f , 99.0f);
		GUI.Label(new Rect(130 , 400 , 200 , 20), "Underbust" , Style);

		waist = GUI.HorizontalSlider(new Rect(80, 490 , 200 , 30) , waist, 34.1f , 99.0f);
		GUI.Label(new Rect(150 , 465 , 200 , 20), "Waist" , Style);

		hips = GUI.HorizontalSlider(new Rect(80, 555 , 200 , 30) , hips, 34.1F , 99.0F);
		GUI.Label(new Rect(155 , 530 , 200 , 20), "Hips" , Style);

		inseam = GUI.HorizontalSlider (new Rect (80, 620 , 200 , 30), inseam, 34.1F , 99.0F);
		GUI.Label (new Rect (140 , 595 , 200 , 20), "Inseam" , Style);

		overBust = GUI.HorizontalSlider(new Rect(80 , 685 , 200 , 30) , overBust, 34.1F , 99.0F);
		GUI.Label(new Rect(135 , 660 , 200 , 20), "Overbust" , Style);

		thighs = GUI.HorizontalSlider(new Rect(80 , 750 , 200 , 30) , thighs, 34.1F , 99.0F);
		GUI.Label(new Rect(135 , 725 , 200 , 20), "Thighs" , Style);

		biceps = GUI.HorizontalSlider(new Rect(80 , 815 , 200 , 30) , biceps, 34.1F , 99.0F);
		GUI.Label(new Rect(135 , 790 , 200 , 20), "Biceps" , Style);



	}

	// Update is called once per frame
	void Update () {

		skinMeshRenderer.SetBlendShapeWeight(0, height - 33.0f);
		skinMeshRenderer.SetBlendShapeWeight(1, neck - 33.0f);
		skinMeshRenderer.SetBlendShapeWeight(2, shoulders - 33.0f);
		skinMeshRenderer.SetBlendShapeWeight (3, waist - 33.0f);
		skinMeshRenderer.SetBlendShapeWeight(4, inseam - 33.0f);
		skinMeshRenderer.SetBlendShapeWeight(5, underBust - 33.0f);
		skinMeshRenderer.SetBlendShapeWeight (6, overBust - 33.0f);
		skinMeshRenderer.SetBlendShapeWeight(7, hips - 33.0f);
		skinMeshRenderer.SetBlendShapeWeight(8, thighs - 33.0f);
		skinMeshRenderer.SetBlendShapeWeight(9, biceps - 33.0f);

		if (height != heightPrevious) {
			heightChange ();
		}
	}

	public void heightChange(){

		heightDiff = (height - heightPrevious)/250;
		if (height >= 50.0f) {
			
			GameObject.Find ("Bip001 L UpperArm").transform.Translate (heightDiff * -0.8f, 0, 0, null);
			GameObject.Find ("Bip001 R UpperArm").transform.Translate (heightDiff * 0.8f, 0, 0, null);

			GameObject.Find ("Bip001 L Forearm").transform.Translate (heightDiff * 0.1f, 0, heightDiff * 0.1f, null);
			GameObject.Find ("Bip001 R Forearm").transform.Translate (heightDiff * -0.1f, 0, heightDiff * 0.1f, null);

			GameObject.Find ("Bip001 L ForeTwist").transform.Translate (0, 0, heightDiff * 0.5f, null);
			GameObject.Find ("Bip001 R ForeTwist").transform.Translate (0, 0, heightDiff * 0.5f, null);

			GameObject.Find ("Bip001 L ForeTwist1").transform.Translate (0, 0, heightDiff * 0.5f, null);
			GameObject.Find ("Bip001 R ForeTwist1").transform.Translate (0, 0, heightDiff * 0.5f, null);

			GameObject.Find ("Bip001 L Forearm").transform.Rotate (0, heightDiff * 50.0f, 0);
			GameObject.Find ("Bip001 R Forearm").transform.Rotate (0, heightDiff * -50.0f, 0);

			GameObject.Find ("Bip001 L ForeTwist").transform.Rotate (0, heightDiff * 10.0f, 0);
			GameObject.Find ("Bip001 R ForeTwist").transform.Rotate (0, heightDiff * -10.0f, 0);

			GameObject.Find ("Bip001 L Hand").transform.Translate (0, heightDiff * -0.7f, heightDiff * 0.3f, Space.Self);
			GameObject.Find ("Bip001 R Hand").transform.Translate (0, heightDiff * -0.7f, heightDiff * 0.3f, Space.Self);

			GameObject.Find ("Bip001 R Finger02").transform.Translate (0, heightDiff * 1.55f, heightDiff * 0.9f, Space.Self);
		
		} else if (height < 50.0f) {

			GameObject.Find ("Bip001 L UpperArm").transform.Translate (heightDiff * -0.8f, 0, 0, null);
			GameObject.Find ("Bip001 R UpperArm").transform.Translate (heightDiff * 0.8f, 0, 0, null);

			GameObject.Find ("Bip001 L Hand").transform.Translate (0, heightDiff * -0.7f, heightDiff * 0.2f, Space.Self);
			GameObject.Find ("Bip001 R Hand").transform.Translate (0, heightDiff * -0.8f, heightDiff * 0.2f, Space.Self);

			GameObject.Find ("Bip001 R Finger02").transform.Translate (0, heightDiff * 1.55f, heightDiff * 0.9f, Space.Self);

		}
		heightPrevious = height;
	}
}
